package tn.esprit.spring;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.services.IEntrepriseService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeTest {
	private static final Logger l = LogManager.getLogger(EmployeTest.class);
		@Autowired
		IEmployeService employeservice;
		
		@Autowired
		EmployeRepository empR;

		@Test
		public void ajouterEmploye() {
			Employe e = new Employe();
			Assert.assertNotEquals(0, employeservice.ajouterEmploye(e));
	}

		@Test
		public void getallEmploye() {

			Assert.assertNotEquals(0, employeservice.getAllEmployes());
			l.info("nombre de liste: {}", employeservice.getAllEmployes().size());

			Integer result = employeservice.getAllEmployes().size();
			Objects.requireNonNull(result); // Compliant
		}

		@Test
		public void deleteEmployebyIdTest() {
			Employe e2 = new Employe("islemm", "louati", "ccc", true, Role.INGENIEUR);
			int idEmploye = employeservice.ajouterEmploye(e2);
			Assert.assertTrue(empR.findById(idEmploye).isPresent());
			employeservice.deleteEmployeById(idEmploye);
			Assert.assertFalse(empR.findById(idEmploye).isPresent());
		}

}

