package tn.esprit.spring.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import tn.esprit.spring.dto.DepartementDTO;
import tn.esprit.spring.entities.Departement;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DepartementMapper {

	DepartementMapper INSTANCE = Mappers.getMapper(DepartementMapper.class);

	Departement departementDTOToDepartement(DepartementDTO departementDTO);

	List<Departement> departementsDTOToDepartements(List<DepartementDTO> departementsDTO);

	DepartementDTO departementToDepartementDTO(Departement departement);

	List<DepartementDTO> departementsToDepartementsDTO(List<Departement> departements);
 
}
