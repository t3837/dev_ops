package tn.esprit.spring.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import tn.esprit.spring.dto.EntrepriseDTO;
import tn.esprit.spring.entities.Entreprise;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EntrepriseMapper {

	EntrepriseMapper INSTANCE = Mappers.getMapper(EntrepriseMapper.class);

	Entreprise entrepriseDTOToEntreprise(EntrepriseDTO entrepriseDTO);

	List<Entreprise> entreprisesDTOToEntreprises(List<EntrepriseDTO> entreprisesDTO);

	EntrepriseDTO entrepriseToEntrepriseDTO(Entreprise entreprise);

	List<EntrepriseDTO> entreprisesToEntreprisesDTO(List<Entreprise> entreprises);

}
