package tn.esprit.spring.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import tn.esprit.spring.dto.EmployeDTO;
import tn.esprit.spring.entities.Employe;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmployeMapper {

	EmployeMapper INSTANCE = Mappers.getMapper(EmployeMapper.class);

	Employe employeDTOToEmploye(EmployeDTO employeDTO);

	List<Employe> employesDTOToEmployes(List<EmployeDTO> employesDTO);

	EmployeDTO employeToEmployeDTO(Employe employe);

	List<EmployeDTO> employesToEmployesDTO(List<Employe> employes);
}
