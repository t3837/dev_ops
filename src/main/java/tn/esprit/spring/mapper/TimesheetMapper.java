package tn.esprit.spring.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import tn.esprit.spring.dto.TimesheetDTO;
import tn.esprit.spring.entities.Timesheet;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TimesheetMapper {

	TimesheetMapper INSTANCE = Mappers.getMapper(TimesheetMapper.class);

	Timesheet timesheetDTOToTimesheet(TimesheetDTO timesheetDTO);

	List<Timesheet> timesheetsDTOToTimesheets(List<TimesheetDTO> timesheetsDTO);

	TimesheetDTO timesheetToTimesheetDTO(Timesheet timesheet);

	List<TimesheetDTO> timesheetsToTimesheetsDTO(List<Timesheet> timesheets);
}
