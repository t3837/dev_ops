package tn.esprit.spring.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import tn.esprit.spring.dto.MissionDTO;
import tn.esprit.spring.entities.Mission;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MissionMapper {

	MissionMapper INSTANCE = Mappers.getMapper(MissionMapper.class);

	Mission missionDTOToMission(MissionDTO missionDTO);

	List<Mission> missionsDTOToMissions(List<MissionDTO> missionsDTO);

	MissionDTO missionToMissionDTO(Mission mission);

	List<MissionDTO> missionsToMissionsDTO(List<Mission> missions);

}
