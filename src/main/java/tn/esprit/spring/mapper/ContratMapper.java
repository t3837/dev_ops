package tn.esprit.spring.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import tn.esprit.spring.dto.ContratDTO;
import tn.esprit.spring.entities.Contrat;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ContratMapper {

	ContratMapper INSTANCE = Mappers.getMapper(ContratMapper.class);

	Contrat contratDTOToContrat(ContratDTO contratDTO);

	List<Contrat> contratsDTOToContrats(List<ContratDTO> contratsDTO);

	ContratDTO contratToContratDTO(Contrat contrat);

	List<ContratDTO> contratsToContratsDTO(List<Contrat> contrats);

}
